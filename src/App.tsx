import React from 'react';
import { TextContainer, Layout, Stack } from '@shopify/polaris';
import { SearchField } from './features/searchField/SearchField';
import { Weather } from './features/wheather/Wheather';
import styles from './App.module.css';

function App() {
    return (
        <div className={styles.root}>
            <Layout>
                <Layout.Section>
                    <Stack vertical>
                        <Weather />
                        <SearchField />
                    </Stack>
                </Layout.Section>
            </Layout>
        </div>
    );
}

export default App;
