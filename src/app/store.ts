import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import currentCityReducer from '../features/currentCity/currentCitySlice';
import searchFieldReducer from '../features/searchField/searchFieldSlice';

export const store = configureStore({
    reducer: {
        counter: counterReducer,
        currentCity: currentCityReducer,
        searchField: searchFieldReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;
