/* global google */

export class GoogleApiProvider {
    inited = false;
    initPromiseResolver?: () => void;
    initPromise = new Promise((resolve) => {
        this.initPromiseResolver = resolve;
    });

    autocompleteService: any;
    placesService: any;
    token: any;

    constructor() {
        // @ts-ignore
        window.googleApiPromise.then(() => {
            this.init();
        });
    }

    init() {
        this.inited = true;
        this.initPromiseResolver?.();
        // @ts-ignore
        this.autocompleteService = new google.maps.places.AutocompleteService();
        // @ts-ignore
        this.placesService = new google.maps.places.PlacesService(
            document.createElement('div')
        );
        // @ts-ignore
        this.token = new google.maps.places.AutocompleteSessionToken();
    }

    getAutocompletePredictions(input: string) {
        let resolvePromise: any;
        const promise = new Promise((resolve) => {
            resolvePromise = resolve;
        });
        this.autocompleteService.getPredictions(
            {
                input,
                sessionToken: this.token,
                types: ['geocode'],
            },
            async (args: any) => {
                if (!args) {
                    resolvePromise([]);
                } else {
                    resolvePromise(
                        await Promise.all(
                            args.map((item: any) =>
                                this.getDetails(item.place_id)
                            )
                        )
                    );
                }
            }
        );

        return promise;
    }

    private getDetails(placeId: string) {
        let resolvePromise: any;
        const promise = new Promise((resolve) => {
            resolvePromise = resolve;
        });
        this.placesService.getDetails({ placeId }, (args: any) => {
            resolvePromise(args);
        });
        return promise;
    }
}
