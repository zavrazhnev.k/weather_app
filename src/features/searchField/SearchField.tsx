import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Autocomplete, AutocompleteProps, TextField } from '@shopify/polaris';
import {
    searchTowns,
    selectSearchFieldLoading,
    selectSearchFieldOptions,
} from './searchFieldSlice';

export function SearchField() {
    const options = useSelector(selectSearchFieldOptions);
    console.log(options);
    const [value, setValue] = useState('');
    const loading = useSelector(selectSearchFieldLoading);
    const dispatch = useDispatch();

    const textField: AutocompleteProps['textField'] = (
        <Autocomplete.TextField
            label={'Select city'}
            onChange={(value) => {
                setValue(value);
                if (value.length > 3) {
                    dispatch(searchTowns(value));
                }
            }}
            value={value}
        />
    );

    return (
        <Autocomplete
            textField={textField}
            loading={loading}
            options={options}
            selected={[]}
            onSelect={(selected: string[]) => {
                console.log(selected);
            }}
        />
    );
}
