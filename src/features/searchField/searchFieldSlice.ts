import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk, RootState } from '../../app/store';
import { AutocompleteProps } from '@shopify/polaris';
import { GoogleApiProvider } from '../../app/googleApiProvider';

interface SearchFieldState {
    options: AutocompleteProps['options'];
    loading: boolean;
}

const initialState: SearchFieldState = {
    options: [{ label: '', value: '' }],
    loading: false,
};

export const searchFieldSlice = createSlice({
    name: 'searchField',
    initialState,
    reducers: {
        setOptions: (
            state,
            action: PayloadAction<SearchFieldState['options']>
        ) => {
            state.options = action.payload;
            state.loading = false;
        },
        setLoading: (state) => {
            state.loading = true;
        },
    },
});

export const { setOptions, setLoading } = searchFieldSlice.actions;

export const searchTowns = (query: string): AppThunk => async (dispatch) => {
    dispatch(setLoading());

    const googleApiProvider = new GoogleApiProvider();
    await googleApiProvider.initPromise;
    const predictions: any = await googleApiProvider.getAutocompletePredictions(
        query
    );

    const options = predictions.filter(Boolean).map((item: any) => {
        return {
            label: item.formatted_address,
            value: item.formatted_address,
            coords: {
                lat: item.geometry.location.lat(),
                lng: item.geometry.location.lng(),
            },
        };
    });
    console.log(options);
    dispatch(setOptions(options));
};

export const selectSearchFieldOptions = (state: RootState) =>
    state.searchField.options;
export const selectSearchFieldLoading = (state: RootState) =>
    state.searchField.loading;

export default searchFieldSlice.reducer;
