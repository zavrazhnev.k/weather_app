import React from 'react';
import { Card, DisplayText } from '@shopify/polaris';

export function Weather() {
    return (
        <Card
            sectioned
            title="Weather forecast"
            actions={[{ content: 'Refresh' }]}
        >
            <DisplayText size="large">SPB</DisplayText>
        </Card>
    );
}
