import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk, RootState } from '../../app/store';
import axios from 'axios';

interface CurrentCityState {
    city: string;
}

const initialState: CurrentCityState = {
    city: '',
};

export const currentCitySlice = createSlice({
    name: 'currentCity',
    initialState,
    reducers: {
        setCity: (state, action: PayloadAction<string>) => {
            state.city = action.payload;
        },
    },
});

const { setCity } = currentCitySlice.actions;

export const setCityByCoords = (lat: number, lng: number): AppThunk => async (
    dispatch
) => {
    //fixme move key to env
    const data = await axios.get(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&result_type=locality&key=AIzaSyCawE8NflrGe1HtnN1egYYEosrTQL39stA`
    );
    console.log(data);
};

export const selectCity = (state: RootState) => state.currentCity.city;

window.navigator.geolocation.getCurrentPosition(async (data) => {
    const { latitude, longitude } = data.coords;
    // setCityByCoords(latitude, longitude);

    const response = await axios.get(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&result_type=locality&key=AIzaSyAuqjrb6B_4YmTbAfQdxIat3xpWA2XyoB4`
    );

    const weather = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?q=${encodeURI(
            response.data.results[0].formatted_address
        )}&appid=6ed93d000e9fdd858d58b138c07b9c90
`
    );

    console.log(weather);
});

export default currentCitySlice.reducer;

// weather api key 6ed93d000e9fdd858d58b138c07b9c90

// google api ket AIzaSyCawE8NflrGe1HtnN1egYYEosrTQL39stA

// get coords by address https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,
// +Mountain+View,+CA&key=YOUR_API_KEY
